# CRUD OPERATION USING MERRN STACK
    - MongoDB, ExpressJs, React, Redux, NodeJs

Restful API's built using Express.js and MongoDB on the server side

## Getting Started

These instructions will get the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the app and how to install them

```
node v8.x, mongodb server
```

### Installing

A step by step guide to get the app running on dev environment

Clone repository

```
git clone git clone https://benigbokwe@bitbucket.org/benigbokwe/memo-example.git
```

```
cd memo-example/
```
```
$ memo-example > npm install
$ memo-example > npm run dev
```

## Test
Coming soon
```
memo-exampl > npm test
```


## Note
- MongoDB server must be started `$ mongod` for the shell or connection from our app server to work - 

## API Endpoints
`http://localhost:3000/api/ideas`

`http://localhost:3000/api/ideas/:id`


## Authors

* **Ben Igbokwe**