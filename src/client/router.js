import React from 'react'

import BrowserRouter from 'react-router-dom/BrowserRouter'
import { renderRoutes } from 'react-router-config'
import { Provider } from 'react-redux'

import routes from '../app/routes'

const AppRouter = ({ store }) => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                {renderRoutes(routes)}
            </BrowserRouter>
        </Provider>
    )
}

export default AppRouter
