import fetch from 'isomorphic-unfetch'
import { apiUrl } from '../../../../package.json'
import {
    addNewMemoRequest,
    addNewMemoRequestSuccess,
    addNewMemoRequestFailed,
    deleteMemoRequest,
    deleteMemoSuccess,
    deleteMemoFailed,
    fetchMemosRequest,
    fetchMemosSuccess,
    fetchMemosFailed,
    fetchMemoRequest,
    fetchMemoSuccess,
    fetchMemoFailed,
    editMemoRequest,
    editMemoSuccess,
    editMemoFailed
} from './create-actions'

import {
    addNotificationSuccess,
    addNotificationFailed
} from '../notification/create-actions'

const endpoint = `http://localhost:3000${apiUrl}/ideas`

export const addNewMemo = (memo) => (dispatch) => {
    dispatch(addNewMemoRequest(memo))
    return fetch(endpoint, {
        method: 'POST',
        body: memo,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
    }).then((response) => {
        if (response.ok) {
            response.json().then((data) => {
                const {
                    message
                } = data
                dispatch(addNewMemoRequestSuccess(data.memo, message))
                dispatch(addNotificationSuccess(message))
            })
        } else {
            response.json().then(error => {
                dispatch(addNotificationFailed(error))
                dispatch(addNewMemoRequestFailed(error))
            })
        }
    })
}

export const deleteMemo = (memo) => (dispatch) => {
    const endpointUrl = `${endpoint}/${memo._id}`
    dispatch(deleteMemoRequest(memo))
    return fetch(endpointUrl, {
        method: 'DELETE'
    }).then(response => {
        if (response.ok) {
            response.json().then((data) => {
                dispatch(deleteMemoSuccess(data.message))
                dispatch(addNotificationSuccess(data.message))
            })
        } else {
            response.json().then(error => {
                dispatch(deleteMemoFailed(error))
                dispatch(addNotificationFailed(error))
            })
        }
    })
}

export const editMemo = (memo) => (dispatch) => {
    dispatch(editMemoRequest(memo))
    return fetch(endpoint, {
        method: 'PUT',
        body: memo,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
    }).then((response) => {
        if (response.ok) {
            response.json().then((data) => {
                dispatch(editMemoSuccess(data.memo))
                dispatch(addNotificationSuccess(data.message))
            })
        } else {
            response.json().then((error) => {
                dispatch(editMemoFailed(error))
                dispatch(addNotificationFailed(error))
            })
        }
    })
}

export const fetchMemos = () => (dispatch) => {
    return new Promise((resolve, reject) => {
        dispatch(fetchMemosRequest())
        return fetch(endpoint)
            .then((response) => {
                if (response.ok) {
                    response.json().then((data) => {
                        const {
                            memos,
                            message
                        } = data
                        resolve(dispatch(fetchMemosSuccess(memos, message)))
                    })
                } else {
                    response.json().then((error) => {
                        reject(dispatch(fetchMemosFailed(error)))
                    })
                }
            })
    })
}

export const fetchMemoById = (memoId) => (dispatch) => {
    const endpointUrl = `${endpoint}/${memoId}`
    dispatch(fetchMemoRequest())
    // Returns a promise
    return fetch(endpointUrl)
        .then((response) => {
            if (response.ok) {
                response.json().then((data) => {
                    const {
                        memo,
                        message
                    } = data

                    dispatch(fetchMemoSuccess(memo, message))
                })
            } else {
                response.json().then((error) => {
                    dispatch(fetchMemoFailed(error))
                })
            }
        })
}
