import * as actions from './constants'

const INITIAL_STATE = {
    memos: [],
    memo: null,
    isFetching: false,
    memoToEdit: null,
}

const memoReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case actions.FETCH_MEMOS_REQUEST:
        return {
            ...state,
            isFetching: true
        }
    case actions.FETCH_MEMOS_SUCCESS:
        return {
            ...state,
            isFetching: false,
            memos: action.memos,
            successMsg: action.message
        }
    case actions.FETCH_MEMOS_FAILED:
        return {
            ...state,
            isFetching: false
        }
    case actions.FETCH_MEMO_REQUEST:
        return {
            ...state,
            memos: state.memos,
            isFetching: true
        }
    case actions.FETCH_MEMO_SUCCESS:
        return {
            ...state,
            memos: state.memos,
            memo: action.memo,
            isFetching: false
        }
    case actions.FETCH_MEMO_FAILED:
        return {
            ...state,
            isFetching: false
        }
    case actions.ADD_NEW_MEMO_REQUEST:
        return {
            ...state,
            memos: state.memos,
            memo: null,
            isFetching: true,
            newMemo: action.memo
        }
    case actions.ADD_NEW_MEMO_REQUEST_FAILED:
        return {
            ...state,
            memos: state.memos,
            memo: null,
            isFetching: true,
            newTodo: null
        }
    case actions.ADD_NEW_MEMO_REQUEST_SUCCESS:
        return {
            ...state,
            memos: [...state.memos, action.memo],
            memo: null,
            isFetching: false,
            newMemo: action.memo
        }
    case actions.EDIT_MEMO_REQUEST:
        return {
            ...state,
            memos: state.memos,
            memo: action.memo,
            isFetching: true
        }
    case actions.EDIT_MEMO_SUCCESS:
        return {
            ...state,
            memos: state.memos.map((memo) => {
                if (memo._id !== action.memo._id) {
                    // This is not the item we care about, keep it as is
                    return memo
                }
                // Otherwise, this is the one we want to return an updated value
                return { ...memo, ...action.memo }
            }),
            memo: null,
            isFetching: false
        }
    case actions.EDIT_MEMO_FAILED:
        return {
            ...state,
            memos: state.memos,
            memo: null,
            isFetching: false,
            memoToEdit: state.memoToEdit,
        }
    case actions.DELETE_MEMO_REQUEST:
        return {
            ...state,
            memos: state.memos,
            memo: action.memo,
            isFetching: true,
        }
    case actions.DELETE_MEMO_SUCCESS:
        return {
            ...state,
            memos: state.memos.filter((memo) => memo._id !== state.memo._id),
            memo: null,
            isFetching: false,
            successMsg: action.message
        }
    case actions.DELETE_MEMO_FAILED:
        return {
            ...state,
            memos: state.memos,
            todo: null,
            isFetching: false
        }
    default:
        return state
    }
}

export default memoReducer
