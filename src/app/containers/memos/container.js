import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import {
    fetchMemos,
    addNewMemo,
    deleteMemo,
    editMemo
} from './actions'
import AddMemoForm from '../../components/form/add-memo-form'
// import EditMemoForm from '../../components/form/edit-memo-form'

import './memos.css'

class Memos extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showAdd: false,
            deleteId: null,
            editableId: null
        }

        this.toggleAddMemo = this.toggleAddMemo.bind(this)
        this.handleAddMemo = this.handleAddMemo.bind(this)
        this.confirmDeleteMemo = this.confirmDeleteMemo.bind(this)
        this.handleEditMemo = this.handleEditMemo.bind(this)
        this.handleMouseEnterOnTile = this.handleMouseEnterOnTile.bind(this)
        this.handleMouseLeaveOnTile = this.handleMouseLeaveOnTile.bind(this)
        this.enableEditable = this.enableEditable.bind(this)
    }

    // @todo Move logic to router table
    static fetchData(store) {
        return store.dispatch(fetchMemos())
    }

    componentWillMount() {
        this.props.fetchMemos()
    }

    toggleAddMemo() {
        this.setState((state) => ({
            showAdd: !state.showAdd
        }))
    }

    handleAddMemo(event) {
        event.preventDefault()
        const form = document.getElementById('addMemoForm')
        const formData = new FormData(form)

        if (form.title.value !== '' && form.body.value !== '') {
            let data = {}
            formData.forEach((value, key) => {
                data[key] = value
            })

            data = JSON.stringify(data)
            this.props.addNewMemo(data)
        }
    }

    confirmDeleteMemo(memo, event) {
        event.preventDefault()
        this.props.deleteMemo(memo)
    }

    enableEditable(event) {
        this.setState({
            editableId: event.target.id
        })
    }

    handleEditMemo(memo, event) {
        event.preventDefault()
        const {
            name,
            value
        } = event.target

        const data = {
            ...memo,
            [name]: value
        }

        this.props.editMemo(JSON.stringify(data))

        this.setState({
            editableId: null
        })
    }

    handleMouseEnterOnTile(delIdx) {
        this.setState({
            deleteId: delIdx
        })
    }

    handleMouseLeaveOnTile() {
        this.setState({
            deleteId: null
        })
    }

    render() {
        const {
            memos,
            isFetching
        } = this.props

        return (
            <div className="m-memos">
                <div>
                    <button onClick={this.toggleAddMemo}>Add New Memo</button>
                    <div>
                        {this.state.showAdd
                            && <AddMemoForm addMemo={this.handleAddMemo} />
                        }
                    </div>
                </div>
                <h3>Memos</h3>
                {
                    !memos && isFetching
                    && <p>Loading memos....</p>
                }
                {
                    memos.length <= 0 && !isFetching
                    && <p>No memos available. Add A Memo to List here.</p>
                }

                {memos && memos.length > 0 && !isFetching
                    && <div className="m-memos-table">
                        <div className="m-memos-table__header-title">
                            <span>Title</span>
                            <span>Body</span>
                        </div>
                        <div>
                            {memos.map((memo, idx) => {
                                const editableTitle = this.state.editableId === `title-${idx}`
                                const editableBody = this.state.editableId === `body-${idx}`
                                const showDeleteIcon = this.state.deleteId === `del-${idx}`

                                return (<div key={idx}
                                    onMouseEnter={() => this.handleMouseEnterOnTile(`del-${idx}`)}
                                    onMouseLeave={() => this.handleMouseLeaveOnTile(`del-${idx}`)}
                                    className="m-memos-table-row-content"
                                >
                                    <div>
                                        <input name='title' type='text' disabled={!editableTitle} defaultValue={memo.title} onBlur={(e) => this.handleEditMemo(memo, e)}/>
                                        <button className="m-memos-table-edit" id={`title-${idx}`} onClick={(e) => this.enableEditable(e)} >Edit</button>
                                    </div>
                                    <div>
                                        <input name='body' type='text' disabled={!editableBody} defaultValue={memo.body} onBlur={(e) => this.handleEditMemo(memo, e)}/>
                                        <button className="m-memos-table-edit" id={`body-${idx}`} onClick={(e) => this.enableEditable(e)} >Edit</button>
                                    </div>
                                    <div><button className="m-memos-table-delete" onClick={(e) => this.confirmDeleteMemo(memo, e)}>Delete</button></div>
                                   
                                    <div><Link to={`/memo/${memo._id}`}>View Details</Link> </div>
                                </div>)
                            })
                            }
                        </div>
                    </div>
                }
            </div>
        )
    }
}

Memos.propTypes = {
    memo: PropTypes.array,
    isFetching: PropTypes.bool
}

const mapStateToProps = (state) => {
    const {
        ui: {
            memo
        }
    } = state

    return {
        memos: memo.memos,
        isFetching: memo.isFetching
    }
}

const mapDispatchToProps = {
    fetchMemos,
    addNewMemo,
    deleteMemo,
    editMemo
}

export default connect(mapStateToProps, mapDispatchToProps)(Memos)
