import React, { Component } from 'react'
import { connect } from 'react-redux'
import { renderRoutes } from 'react-router-config'
import {
    fetchMemoById
} from '../memos/actions'

class Memo extends Component {
    componentWillMount() {
        const memoId = this.props.match.params.id
        this.props.fetchMemoById(memoId)
    }

    render() {
        const {
            memo,
            isFetching,
            route
        } = this.props

        return (
            <div>
                <h2>Memo Detail</h2>
                {!memo && isFetching
                    && <div>
                        <p>Loading memo....</p>
                    </div>
                }
                {memo && !isFetching
                && <div>
                    <h3>{memo.created_date}</h3>
                    <p>{memo.title}</p>
                    <p>{memo.body}</p>
                </div>
                }
                {renderRoutes(route.routes)}
            </div>
        )
    }
}

// map state from store to props
const mapStateToProps = (state) => {
    const {
        ui: {
            memo
        }
    } = state

    return {
        memo: memo.memo,
        isFetching: memo.isFetching
    }
}

// map actions to props
const mapDispatchToProps = {
    fetchMemoById
}

export default connect(mapStateToProps, mapDispatchToProps)(Memo)
