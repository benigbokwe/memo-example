import React from 'react'
import { renderRoutes } from 'react-router-config'

const Testpage = ({ route }) => {
    return (
        <div>
            <h2>Child Test</h2>
            {/* child routes won't render without this */}
            {renderRoutes(route.routes, { someProp: 'these extra props are optional' })}
        </div>
    )
}

export default Testpage
