import {
    createStore,
    applyMiddleware,
    combineReducers
} from 'redux'
import thunk from 'redux-thunk'
import { reducer as formReducer } from 'redux-form'
import rootReducer from '../reducers'

const reducer = combineReducers({
    ui: rootReducer,
    form: formReducer
})

const configureStore = (preloadedState) => (
    createStore(reducer, preloadedState, applyMiddleware(thunk))
)

export default configureStore
