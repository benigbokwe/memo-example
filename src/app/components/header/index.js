import React from 'react'
import { Link } from 'react-router-dom'

import Notification from '../../containers/notification/container'

import './header.css'

const Header = () => {
    return (
        <div className='m-header'>
            <ul className='m-header-list'>
                <li>
                    <Link to="/">Memo - code challege</Link>
                </li>
                <li>
                    <Link to="/test">Test</Link>
                </li>
            </ul>
            <Notification />
        </div>
    )
}

export default Header
