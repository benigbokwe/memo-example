
import { combineReducers } from 'redux'
import memoReducer from '../containers/memos/reducer'
import notificationReducer from '../containers/notification/reducer'

const rootReducer = combineReducers({
    memo: memoReducer,
    notification: notificationReducer
})

export default rootReducer
