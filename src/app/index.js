import React from 'react'
import PropTypes from 'prop-types'
import { renderRoutes } from 'react-router-config'

// import containers
import Header from './components/header'
import Footer from './components/footer'

import './app.css'

const App = ({ route }) => {
    return (
        <div className="container">
            <Header />
            <main role="main">
                {renderRoutes(route.routes)}
            </main>
            <Footer />
        </div>
    )
}

App.propTypes = {
    route: PropTypes.object
}

export default App
