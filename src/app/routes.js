import App from '.'
import Memo from './containers/memo/container'
import Memos from './containers/memos/container'
import Testpage from './containers/test/container'

const routes = [
    {
        component: App,
        routes: [
            {
                path: '/',
                exact: true,
                component: Memos
            },
            {
                path: '/memo/:id',
                component: Memo
            },
            {
                path: '/test',
                component: Testpage
            }
        ]
    }
]

export default routes
