import express from 'express'
import bodyParser from 'body-parser'

// import controller file
import {
    getMemos,
    addMemo,
    updateMemo,
    getMemo,
    deleteMemo
} from './controllers/memo'

// get an instance of express router
const router = express.Router()
// parse various different custom JSON types as JSON
router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: false }))

router.route('/ideas')
    .get(getMemos)
    .post(addMemo)
    .put(updateMemo)
router.route('/ideas/:id')
    .get(getMemo)
    .delete(deleteMemo)

export default router
