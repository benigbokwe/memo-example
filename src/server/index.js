import express from 'express'
// import path from 'path'
import React from 'react'
import { renderToString } from 'react-dom/server'
import StaticRouter from 'react-router-dom/StaticRouter'
import { renderRoutes, matchRoutes } from 'react-router-config'
import { Provider } from 'react-redux'
// import logger from 'morgan'
import mongoose from 'mongoose'
import routes from '../app/routes'
import configureStore from '../app/store'
import router from './route'

const app = express()

// allow-cors
// configure app
// serve static files
app.use(express.static('public'))
// set the port
const port = process.env.PORT || 3000

const renderFullPage = (html, preloadedState) => {
    return `
      <!doctype html>
      <html>
        <head>
          <title>Memo technical challenge</title>
          <link rel="stylesheet" href="/css/main.css">
        </head>
        <body>
          <div id="root">${html}</div>
          <script>
            // WARNING: See the following for security issues around embedding JSON in HTML:
            // http://redux.js.org/recipes/ServerRendering.html#security-considerations
            window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
          </script>
          <script src="/bundle.js"></script>
        </body>
      </html>
    `
}

const handleRender = (req, res) => {
    const context = {}
    // Create a new Redux store instance
    const store = configureStore(context)
    const branch = matchRoutes(routes, req.url)
    const promises = branch.map(({ route }) => {
        const {
            component: {
                fetchData
            }
        } = route
        return fetchData instanceof Function ? fetchData(store) : Promise.resolve(null)
    })

    return Promise.all(promises).then(() => {
        // Render the component to a string
        const html = renderToString(
            <Provider store={store}>
                <StaticRouter location={req.url} context={context}>
                    {renderRoutes(routes)}
                </StaticRouter>
            </Provider>
        )

        // Grab the initial state from our Redux store
        const finalState = store.getState()
        // Send the rendered page back to the client
        res.send(renderFullPage(html, finalState))
    })
}

// connect to database
mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost/memo-app')
app.use('/api', router)
// This is fired every time the server side receives a request
app.use(handleRender)

// start the server
app.listen(port, () => {
    console.log(`App Server Listening at ${port}`)
})
