import mongoose, { Schema } from 'mongoose'

const MemoSchema = new Schema({
    created_date: {
        type: Date,
        default: Date.now
    },
    title: String,
    body: String
})

export default mongoose.model('Memo', MemoSchema)
