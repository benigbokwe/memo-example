// import models
import Memo from '../models/memo'

export const getMemos = (req, res) => {
    Memo.find().exec((err, memos) => {
        if (err) {
            return res.json({
                success: false,
                message: 'Error occured'
            })
        }

        return res.json({
            success: true,
            message: 'Memos fetched successfully',
            memos
        })
    })
}

export const addMemo = (req, res) => {
    const {
        body
    } = req

    const newMemo = new Memo(body)
    newMemo.save((err, memo) => {
        if (err) {
            return res.json({
                success: false,
                message: 'Error occured'
            })
        }

        return res.json({
            success: true,
            message: 'Memo added successfully',
            memo
        })
    })
}

export const updateMemo = (req, res) => {
    Memo.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, (err, memo) => {
        if (err) {
            return res.json({
                success: false,
                message: 'Error occured',
                error: err
            })
        }

        return res.json({
            success: true,
            message: 'Updated successfully',
            memo
        })
    })
}

export const getMemo = (req, res) => {
    Memo.find({ _id: req.params.id }).exec((err, memo) => {
        if (err) {
            return res.json({
                success: false,
                message: 'Error occured'
            })
        }

        if (memo.length) {
            const mem = memo[0]
            return res.json({
                success: true,
                message: 'Memo fetched by id successfully',
                memo: mem
            })
        }

        return res.json({
            success: false,
            message: 'Memo with the given id not found'
        })
    })
}

export const deleteMemo = (req, res) => {
    Memo.findByIdAndRemove(req.params.id, (err) => {
        if (err) {
            return res.json({
                success: false,
                message: 'Error occured'
            })
        }

        return res.json({
            success: true,
            message: 'deleted successfully'
        })
    })
}
