const webpack = require("webpack")
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const nodeExternals = require('webpack-node-externals')
// const autoprefixer = require('autoprefixer')

const clientConfig = {
    entry: './src/client/index.js',
    output: {
        path: __dirname,
        filename: './public/bundle.js'
    },
    devtool: 'cheap-module-source-map',
    module: {
        rules: [{
            test: [/\.svg$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
            loader: 'file-loader',
            options: {
                name: 'public/media/[name].[ext]',
                publicPath: url => url.replace(/public/, '')
            }
        },
        {
            test: /\.css$/,
            use: [
                MiniCssExtractPlugin.loader,
                // 'style-loader',
                'css-loader'
            ]
        },
        {
            test: /\.(js|jsx)$/,
            exclude: /(node_modules)/,
            loader: 'babel-loader'
        }]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'public/css/[name].css',
            chunkFilename: '[id].css'
        }),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'window.$': 'jquery',
            'popper.js': 'popper.js',
            'bootstrap': 'bootstrap'
        })
    ]
}

const serverConfig = {
    entry: './src/server/index.js',
    target: 'node', // in order to ignore built-in modules like path, fs, etc.
    externals: [nodeExternals()], // in order to ignore all modules in node_modules folder
    output: {
        path: __dirname,
        filename: 'server.js',
        libraryTarget: 'commonjs2'
    },
    devtool: 'cheap-module-source-map',
    module: {
        rules: [{
            test: [/\.svg$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
            loader: 'file-loader',
            options: {
                name: 'public/media/[name].[ext]',
                publicPath: url => url.replace(/public/, ''),
                emit: false
            }
        },
        {
            test: /\.css$/,
            use: [{
                loader: 'css-loader/locals'
            }]
        },
        {
            test: /\.(js|jsx)$/,
            exclude: /(node_modules)/,
            loader: 'babel-loader'
        }]
    }
}

module.exports = [clientConfig, serverConfig]
