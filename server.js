module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/server/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./package.json":
/*!**********************!*\
  !*** ./package.json ***!
  \**********************/
/*! exports provided: name, version, description, main, apiUrl, scripts, keywords, author, license, dependencies, devDependencies, default */
/***/ (function(module) {

module.exports = {"name":"memo","version":"0.1.0","description":"","main":"server.js","apiUrl":"/api","scripts":{"start":"webpack -w --mode development --open","dev":"npm run start & nodemon server.js","build":"webpack --mode production","test":"echo \"Error: no test specified\" && exit 1"},"keywords":[],"author":"","license":"ISC","dependencies":{"body-parser":"^1.18.3","es6-promise":"^4.2.5","express":"^4.16.3","isomorphic-unfetch":"^2.1.1","mongoose":"^5.2.14","morgan":"^1.9.1","prop-types":"^15.6.2","react":"^16.5.0","react-dom":"^16.5.0","react-redux":"^5.0.7","react-router":"^4.3.1","react-router-config":"^1.0.0-beta.4","react-router-dom":"^4.3.1","redux":"^4.0.0","redux-form":"^7.4.2","redux-thunk":"^2.3.0"},"devDependencies":{"@babel/core":"^7.0.1","@babel/preset-env":"^7.0.0","@babel/preset-react":"^7.0.0","autoprefixer":"^9.1.5","babel-loader":"^8.0.2","babel-preset-react":"^6.24.1","cross-env":"^5.2.0","css-loader":"^1.0.0","eslint":"^5.6.0","eslint-config-airbnb":"^17.1.0","eslint-plugin-import":"^2.14.0","eslint-plugin-jsx-a11y":"^6.1.1","eslint-plugin-react":"^7.11.1","file-loader":"^2.0.0","jquery":"^3.3.1","mini-css-extract-plugin":"^0.4.2","nodemon":"^1.18.4","popper.js":"^1.14.4","postcss-loader":"^3.0.0","style-loader":"^0.23.0","webpack":"^4.18.1","webpack-cli":"^3.1.0","webpack-node-externals":"^1.7.2"}};

/***/ }),

/***/ "./src/app/app.css":
/*!*************************!*\
  !*** ./src/app/app.css ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./src/app/components/footer/footer.css":
/*!**********************************************!*\
  !*** ./src/app/components/footer/footer.css ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./src/app/components/footer/index.js":
/*!********************************************!*\
  !*** ./src/app/components/footer/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _footer_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./footer.css */ "./src/app/components/footer/footer.css");
/* harmony import */ var _footer_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_footer_css__WEBPACK_IMPORTED_MODULE_1__);



var Footer = function Footer() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "m-footer"
  }, "footer");
};

/* harmony default export */ __webpack_exports__["default"] = (Footer);

/***/ }),

/***/ "./src/app/components/form/add-memo-form.css":
/*!***************************************************!*\
  !*** ./src/app/components/form/add-memo-form.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./src/app/components/form/add-memo-form.js":
/*!**************************************************!*\
  !*** ./src/app/components/form/add-memo-form.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _add_memo_form_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./add-memo-form.css */ "./src/app/components/form/add-memo-form.css");
/* harmony import */ var _add_memo_form_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_add_memo_form_css__WEBPACK_IMPORTED_MODULE_1__);



var AddMemoForm = function AddMemoForm(_ref) {
  var addMemo = _ref.addMemo;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "m-add-memo-form"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    onSubmit: addMemo,
    id: "addMemoForm"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "row"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, "Add Memo"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
    for: "title"
  }, "Title: "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "text",
    placeholder: "Enter title here...",
    name: "title"
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", null, "Description: "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("textarea", {
    name: "body",
    placeholder: "Enter body description here..."
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: "m-btn-primary"
  }, "Submit"))));
};

/* harmony default export */ __webpack_exports__["default"] = (AddMemoForm);

/***/ }),

/***/ "./src/app/components/header/header.css":
/*!**********************************************!*\
  !*** ./src/app/components/header/header.css ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./src/app/components/header/index.js":
/*!********************************************!*\
  !*** ./src/app/components/header/index.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _containers_notification_container__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../containers/notification/container */ "./src/app/containers/notification/container.js");
/* harmony import */ var _header_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./header.css */ "./src/app/components/header/header.css");
/* harmony import */ var _header_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_header_css__WEBPACK_IMPORTED_MODULE_3__);





var Header = function Header() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "m-header"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "m-header-list"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: "/"
  }, "Memo - code challege")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: "/test"
  }, "Test"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_containers_notification_container__WEBPACK_IMPORTED_MODULE_2__["default"], null));
};

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./src/app/containers/memo/container.js":
/*!**********************************************!*\
  !*** ./src/app/containers/memo/container.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-config */ "react-router-config");
/* harmony import */ var react_router_config__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_router_config__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _memos_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../memos/actions */ "./src/app/containers/memos/actions.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






var Memo =
/*#__PURE__*/
function (_Component) {
  _inherits(Memo, _Component);

  function Memo() {
    _classCallCheck(this, Memo);

    return _possibleConstructorReturn(this, _getPrototypeOf(Memo).apply(this, arguments));
  }

  _createClass(Memo, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      var memoId = this.props.match.params.id;
      this.props.fetchMemoById(memoId);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          memo = _this$props.memo,
          isFetching = _this$props.isFetching,
          route = _this$props.route;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", null, "Memo Detail"), !memo && isFetching && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Loading memo....")), memo && !isFetching && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, memo.created_date), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, memo.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, memo.body)), Object(react_router_config__WEBPACK_IMPORTED_MODULE_2__["renderRoutes"])(route.routes));
    }
  }]);

  return Memo;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]); // map state from store to props


var mapStateToProps = function mapStateToProps(state) {
  var memo = state.ui.memo;
  return {
    memo: memo.memo,
    isFetching: memo.isFetching
  };
}; // map actions to props


var mapDispatchToProps = {
  fetchMemoById: _memos_actions__WEBPACK_IMPORTED_MODULE_3__["fetchMemoById"]
};
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps, mapDispatchToProps)(Memo));

/***/ }),

/***/ "./src/app/containers/memos/actions.js":
/*!*********************************************!*\
  !*** ./src/app/containers/memos/actions.js ***!
  \*********************************************/
/*! exports provided: addNewMemo, deleteMemo, editMemo, fetchMemos, fetchMemoById */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addNewMemo", function() { return addNewMemo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteMemo", function() { return deleteMemo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editMemo", function() { return editMemo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchMemos", function() { return fetchMemos; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchMemoById", function() { return fetchMemoById; });
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! isomorphic-unfetch */ "isomorphic-unfetch");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _package_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../package.json */ "./package.json");
var _package_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../../package.json */ "./package.json", 1);
/* harmony import */ var _create_actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./create-actions */ "./src/app/containers/memos/create-actions.js");
/* harmony import */ var _notification_create_actions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../notification/create-actions */ "./src/app/containers/notification/create-actions.js");




var endpoint = "http://localhost:3000".concat(_package_json__WEBPACK_IMPORTED_MODULE_1__["apiUrl"], "/ideas");
var addNewMemo = function addNewMemo(memo) {
  return function (dispatch) {
    dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["addNewMemoRequest"])(memo));
    return isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_0___default()(endpoint, {
      method: 'POST',
      body: memo,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(function (response) {
      if (response.ok) {
        response.json().then(function (data) {
          var message = data.message;
          dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["addNewMemoRequestSuccess"])(data.memo, message));
          dispatch(Object(_notification_create_actions__WEBPACK_IMPORTED_MODULE_3__["addNotificationSuccess"])(message));
        });
      } else {
        response.json().then(function (error) {
          dispatch(Object(_notification_create_actions__WEBPACK_IMPORTED_MODULE_3__["addNotificationFailed"])(error));
          dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["addNewMemoRequestFailed"])(error));
        });
      }
    });
  };
};
var deleteMemo = function deleteMemo(memo) {
  return function (dispatch) {
    var endpointUrl = "".concat(endpoint, "/").concat(memo._id);
    dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["deleteMemoRequest"])(memo));
    return isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_0___default()(endpointUrl, {
      method: 'DELETE'
    }).then(function (response) {
      if (response.ok) {
        response.json().then(function (data) {
          dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["deleteMemoSuccess"])(data.message));
          dispatch(Object(_notification_create_actions__WEBPACK_IMPORTED_MODULE_3__["addNotificationSuccess"])(data.message));
        });
      } else {
        response.json().then(function (error) {
          dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["deleteMemoFailed"])(error));
          dispatch(Object(_notification_create_actions__WEBPACK_IMPORTED_MODULE_3__["addNotificationFailed"])(error));
        });
      }
    });
  };
};
var editMemo = function editMemo(memo) {
  return function (dispatch) {
    dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["editMemoRequest"])(memo));
    return isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_0___default()(endpoint, {
      method: 'PUT',
      body: memo,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(function (response) {
      if (response.ok) {
        response.json().then(function (data) {
          dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["editMemoSuccess"])(data.memo));
          dispatch(Object(_notification_create_actions__WEBPACK_IMPORTED_MODULE_3__["addNotificationSuccess"])(data.message));
        });
      } else {
        response.json().then(function (error) {
          dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["editMemoFailed"])(error));
          dispatch(Object(_notification_create_actions__WEBPACK_IMPORTED_MODULE_3__["addNotificationFailed"])(error));
        });
      }
    });
  };
};
var fetchMemos = function fetchMemos() {
  return function (dispatch) {
    return new Promise(function (resolve, reject) {
      dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["fetchMemosRequest"])());
      return isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_0___default()(endpoint).then(function (response) {
        if (response.ok) {
          response.json().then(function (data) {
            var memos = data.memos,
                message = data.message;
            resolve(dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["fetchMemosSuccess"])(memos, message)));
          });
        } else {
          response.json().then(function (error) {
            reject(dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["fetchMemosFailed"])(error)));
          });
        }
      });
    });
  };
};
var fetchMemoById = function fetchMemoById(memoId) {
  return function (dispatch) {
    var endpointUrl = "".concat(endpoint, "/").concat(memoId);
    dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["fetchMemoRequest"])()); // Returns a promise

    return isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_0___default()(endpointUrl).then(function (response) {
      if (response.ok) {
        response.json().then(function (data) {
          var memo = data.memo,
              message = data.message;
          dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["fetchMemoSuccess"])(memo, message));
        });
      } else {
        response.json().then(function (error) {
          dispatch(Object(_create_actions__WEBPACK_IMPORTED_MODULE_2__["fetchMemoFailed"])(error));
        });
      }
    });
  };
};

/***/ }),

/***/ "./src/app/containers/memos/constants.js":
/*!***********************************************!*\
  !*** ./src/app/containers/memos/constants.js ***!
  \***********************************************/
/*! exports provided: ADD_MEMO, UPDATE_MEMO, DELETE_MEMO, DELETE_MEMO_REQUEST, DELETE_MEMO_SUCCESS, DELETE_MEMO_FAILED, READ_MEMO, FETCH_MEMO_REQUEST, FETCH_MEMOS_REQUEST, FETCH_MEMO_SUCCESS, FETCH_MEMO_FAILED, FETCH_MEMOS_SUCCESS, FETCH_MEMOS_FAILED, TOGGLE_ADD_MEMO, ADD_NEW_MEMO_REQUEST, ADD_NEW_MEMO_REQUEST_SUCCESS, ADD_NEW_MEMO_REQUEST_FAILED, EDIT_MEMO_REQUEST, EDIT_MEMO_SUCCESS, EDIT_MEMO_FAILED */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_MEMO", function() { return ADD_MEMO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_MEMO", function() { return UPDATE_MEMO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_MEMO", function() { return DELETE_MEMO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_MEMO_REQUEST", function() { return DELETE_MEMO_REQUEST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_MEMO_SUCCESS", function() { return DELETE_MEMO_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_MEMO_FAILED", function() { return DELETE_MEMO_FAILED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "READ_MEMO", function() { return READ_MEMO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_MEMO_REQUEST", function() { return FETCH_MEMO_REQUEST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_MEMOS_REQUEST", function() { return FETCH_MEMOS_REQUEST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_MEMO_SUCCESS", function() { return FETCH_MEMO_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_MEMO_FAILED", function() { return FETCH_MEMO_FAILED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_MEMOS_SUCCESS", function() { return FETCH_MEMOS_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FETCH_MEMOS_FAILED", function() { return FETCH_MEMOS_FAILED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TOGGLE_ADD_MEMO", function() { return TOGGLE_ADD_MEMO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_NEW_MEMO_REQUEST", function() { return ADD_NEW_MEMO_REQUEST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_NEW_MEMO_REQUEST_SUCCESS", function() { return ADD_NEW_MEMO_REQUEST_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_NEW_MEMO_REQUEST_FAILED", function() { return ADD_NEW_MEMO_REQUEST_FAILED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EDIT_MEMO_REQUEST", function() { return EDIT_MEMO_REQUEST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EDIT_MEMO_SUCCESS", function() { return EDIT_MEMO_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EDIT_MEMO_FAILED", function() { return EDIT_MEMO_FAILED; });
var ADD_MEMO = 'ADD_MEMO';
var UPDATE_MEMO = 'UPDATE_MEMO';
var DELETE_MEMO = 'DELETE_MEMO';
var DELETE_MEMO_REQUEST = 'DELETE_MEMO_REQUEST';
var DELETE_MEMO_SUCCESS = 'DELETE_MEMO_SUCCESS';
var DELETE_MEMO_FAILED = 'DELETE_MEMO_FAILED';
var READ_MEMO = 'READ_MEMO';
var FETCH_MEMO_REQUEST = 'FETCH_MEMO_REQUEST';
var FETCH_MEMOS_REQUEST = 'FETCH_MEMOS_REQUEST';
var FETCH_MEMO_SUCCESS = 'FETCH_MEMO_SUCCESS';
var FETCH_MEMO_FAILED = 'FETCH_MEMO_FAILED';
var FETCH_MEMOS_SUCCESS = 'FETCH_MEMOS_SUCCESS';
var FETCH_MEMOS_FAILED = 'FETCH_MEMOS_FAILED';
var TOGGLE_ADD_MEMO = 'TOGGLE_ADD_MEMO';
var ADD_NEW_MEMO_REQUEST = 'ADD_NEW_MEMO_REQUEST';
var ADD_NEW_MEMO_REQUEST_SUCCESS = 'ADD_NEW_MEMO_REQUEST_SUCCESS';
var ADD_NEW_MEMO_REQUEST_FAILED = 'ADD_NEW_MEMO_REQUEST_FAILED';
var EDIT_MEMO_REQUEST = 'EDIT_MEMO_REQUEST';
var EDIT_MEMO_SUCCESS = 'EDIT_MEMO_SUCCESS';
var EDIT_MEMO_FAILED = 'EDIT_MEMO_FAILED';

/***/ }),

/***/ "./src/app/containers/memos/container.js":
/*!***********************************************!*\
  !*** ./src/app/containers/memos/container.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./actions */ "./src/app/containers/memos/actions.js");
/* harmony import */ var _components_form_add_memo_form__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/form/add-memo-form */ "./src/app/components/form/add-memo-form.js");
/* harmony import */ var _memos_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./memos.css */ "./src/app/containers/memos/memos.css");
/* harmony import */ var _memos_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_memos_css__WEBPACK_IMPORTED_MODULE_6__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }






 // import EditMemoForm from '../../components/form/edit-memo-form'



var Memos =
/*#__PURE__*/
function (_Component) {
  _inherits(Memos, _Component);

  function Memos(props) {
    var _this;

    _classCallCheck(this, Memos);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Memos).call(this, props));
    _this.state = {
      showAdd: false,
      deleteId: null,
      editableId: null
    };
    _this.toggleAddMemo = _this.toggleAddMemo.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleAddMemo = _this.handleAddMemo.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.confirmDeleteMemo = _this.confirmDeleteMemo.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleEditMemo = _this.handleEditMemo.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleMouseEnterOnTile = _this.handleMouseEnterOnTile.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleMouseLeaveOnTile = _this.handleMouseLeaveOnTile.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.enableEditable = _this.enableEditable.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  } // @todo Move logic to router table


  _createClass(Memos, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      this.props.fetchMemos();
    }
  }, {
    key: "toggleAddMemo",
    value: function toggleAddMemo() {
      this.setState(function (state) {
        return {
          showAdd: !state.showAdd
        };
      });
    }
  }, {
    key: "handleAddMemo",
    value: function handleAddMemo(event) {
      event.preventDefault();
      var form = document.getElementById('addMemoForm');
      var formData = new FormData(form);

      if (form.title.value !== '' && form.body.value !== '') {
        var data = {};
        formData.forEach(function (value, key) {
          data[key] = value;
        });
        data = JSON.stringify(data);
        this.props.addNewMemo(data);
      }
    }
  }, {
    key: "confirmDeleteMemo",
    value: function confirmDeleteMemo(memo, event) {
      event.preventDefault();
      this.props.deleteMemo(memo);
    }
  }, {
    key: "enableEditable",
    value: function enableEditable(event) {
      this.setState({
        editableId: event.target.id
      });
    }
  }, {
    key: "handleEditMemo",
    value: function handleEditMemo(memo, event) {
      event.preventDefault();
      var _event$target = event.target,
          name = _event$target.name,
          value = _event$target.value;

      var data = _objectSpread({}, memo, _defineProperty({}, name, value));

      this.props.editMemo(JSON.stringify(data));
      this.setState({
        editableId: null
      });
    }
  }, {
    key: "handleMouseEnterOnTile",
    value: function handleMouseEnterOnTile(delIdx) {
      this.setState({
        deleteId: delIdx
      });
    }
  }, {
    key: "handleMouseLeaveOnTile",
    value: function handleMouseLeaveOnTile() {
      this.setState({
        deleteId: null
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          memos = _this$props.memos,
          isFetching = _this$props.isFetching;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "m-memos"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.toggleAddMemo
      }, "Add New Memo"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, this.state.showAdd && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_form_add_memo_form__WEBPACK_IMPORTED_MODULE_5__["default"], {
        addMemo: this.handleAddMemo
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", null, "Memos"), !memos && isFetching && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Loading memos...."), memos.length <= 0 && !isFetching && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "No memos available. Add A Memo to List here."), memos && memos.length > 0 && !isFetching && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "m-memos-table"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "m-memos-table__header-title"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Title"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "Body")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, memos.map(function (memo, idx) {
        var editableTitle = _this2.state.editableId === "title-".concat(idx);
        var editableBody = _this2.state.editableId === "body-".concat(idx);
        var showDeleteIcon = _this2.state.deleteId === "del-".concat(idx);
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          key: idx,
          onMouseEnter: function onMouseEnter() {
            return _this2.handleMouseEnterOnTile("del-".concat(idx));
          },
          onMouseLeave: function onMouseLeave() {
            return _this2.handleMouseLeaveOnTile("del-".concat(idx));
          },
          className: "m-memos-table-row-content"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
          name: "title",
          type: "text",
          disabled: !editableTitle,
          defaultValue: memo.title,
          onBlur: function onBlur(e) {
            return _this2.handleEditMemo(memo, e);
          }
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          className: "m-memos-table-edit",
          id: "title-".concat(idx),
          onClick: function onClick(e) {
            return _this2.enableEditable(e);
          }
        }, "Edit")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
          name: "body",
          type: "text",
          disabled: !editableBody,
          defaultValue: memo.body,
          onBlur: function onBlur(e) {
            return _this2.handleEditMemo(memo, e);
          }
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          className: "m-memos-table-edit",
          id: "body-".concat(idx),
          onClick: function onClick(e) {
            return _this2.enableEditable(e);
          }
        }, "Edit")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
          className: "m-memos-table-delete",
          onClick: function onClick(e) {
            return _this2.confirmDeleteMemo(memo, e);
          }
        }, "Delete")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
          to: "/memo/".concat(memo._id)
        }, "View Details"), " "));
      }))));
    }
  }], [{
    key: "fetchData",
    value: function fetchData(store) {
      return store.dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_4__["fetchMemos"])());
    }
  }]);

  return Memos;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

Memos.propTypes = {
  memo: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.array,
  isFetching: prop_types__WEBPACK_IMPORTED_MODULE_3___default.a.bool
};

var mapStateToProps = function mapStateToProps(state) {
  var memo = state.ui.memo;
  return {
    memos: memo.memos,
    isFetching: memo.isFetching
  };
};

var mapDispatchToProps = {
  fetchMemos: _actions__WEBPACK_IMPORTED_MODULE_4__["fetchMemos"],
  addNewMemo: _actions__WEBPACK_IMPORTED_MODULE_4__["addNewMemo"],
  deleteMemo: _actions__WEBPACK_IMPORTED_MODULE_4__["deleteMemo"],
  editMemo: _actions__WEBPACK_IMPORTED_MODULE_4__["editMemo"]
};
/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps, mapDispatchToProps)(Memos));

/***/ }),

/***/ "./src/app/containers/memos/create-actions.js":
/*!****************************************************!*\
  !*** ./src/app/containers/memos/create-actions.js ***!
  \****************************************************/
/*! exports provided: addNewMemoRequest, addNewMemoRequestSuccess, addNewMemoRequestFailed, fetchMemosRequest, fetchMemoRequest, fetchMemoSuccess, fetchMemosSuccess, fetchMemosFailed, fetchMemoFailed, deleteMemoRequest, deleteMemoSuccess, deleteMemoFailed, editMemoRequest, editMemoSuccess, editMemoFailed */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addNewMemoRequest", function() { return addNewMemoRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addNewMemoRequestSuccess", function() { return addNewMemoRequestSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addNewMemoRequestFailed", function() { return addNewMemoRequestFailed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchMemosRequest", function() { return fetchMemosRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchMemoRequest", function() { return fetchMemoRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchMemoSuccess", function() { return fetchMemoSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchMemosSuccess", function() { return fetchMemosSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchMemosFailed", function() { return fetchMemosFailed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetchMemoFailed", function() { return fetchMemoFailed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteMemoRequest", function() { return deleteMemoRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteMemoSuccess", function() { return deleteMemoSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteMemoFailed", function() { return deleteMemoFailed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editMemoRequest", function() { return editMemoRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editMemoSuccess", function() { return editMemoSuccess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editMemoFailed", function() { return editMemoFailed; });
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./constants */ "./src/app/containers/memos/constants.js");

var addNewMemoRequest = function addNewMemoRequest(memo) {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["ADD_NEW_MEMO_REQUEST"],
    memo: memo
  };
};
var addNewMemoRequestSuccess = function addNewMemoRequestSuccess(memo) {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["ADD_NEW_MEMO_REQUEST_SUCCESS"],
    memo: memo
  };
};
var addNewMemoRequestFailed = function addNewMemoRequestFailed() {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["ADD_NEW_MEMO_REQUEST_FAILED"]
  };
};
var fetchMemosRequest = function fetchMemosRequest() {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["FETCH_MEMOS_REQUEST"]
  };
};
var fetchMemoRequest = function fetchMemoRequest() {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["FETCH_MEMO_REQUEST"]
  };
};
var fetchMemoSuccess = function fetchMemoSuccess(memo) {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["FETCH_MEMO_SUCCESS"],
    memo: memo
  };
};
var fetchMemosSuccess = function fetchMemosSuccess(memos) {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["FETCH_MEMOS_SUCCESS"],
    memos: memos
  };
};
var fetchMemosFailed = function fetchMemosFailed() {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["FETCH_MEMOS_FAILED"]
  };
};
var fetchMemoFailed = function fetchMemoFailed() {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["FETCH_MEMO_FAILED"]
  };
};
var deleteMemoRequest = function deleteMemoRequest(memo) {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["DELETE_MEMO_REQUEST"],
    memo: memo
  };
};
var deleteMemoSuccess = function deleteMemoSuccess() {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["DELETE_MEMO_SUCCESS"]
  };
};
var deleteMemoFailed = function deleteMemoFailed() {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["DELETE_MEMO_FAILED"]
  };
};
var editMemoRequest = function editMemoRequest(memo) {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["EDIT_MEMO_REQUEST"],
    memo: memo
  };
};
var editMemoSuccess = function editMemoSuccess(memo) {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["EDIT_MEMO_SUCCESS"],
    memo: memo
  };
};
var editMemoFailed = function editMemoFailed() {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["EDIT_MEMO_FAILED"]
  };
};

/***/ }),

/***/ "./src/app/containers/memos/memos.css":
/*!********************************************!*\
  !*** ./src/app/containers/memos/memos.css ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./src/app/containers/memos/reducer.js":
/*!*********************************************!*\
  !*** ./src/app/containers/memos/reducer.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./constants */ "./src/app/containers/memos/constants.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var INITIAL_STATE = {
  memos: [],
  memo: null,
  isFetching: false,
  memoToEdit: null
};

var memoReducer = function memoReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _constants__WEBPACK_IMPORTED_MODULE_0__["FETCH_MEMOS_REQUEST"]:
      return _objectSpread({}, state, {
        isFetching: true
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["FETCH_MEMOS_SUCCESS"]:
      return _objectSpread({}, state, {
        isFetching: false,
        memos: action.memos,
        successMsg: action.message
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["FETCH_MEMOS_FAILED"]:
      return _objectSpread({}, state, {
        isFetching: false
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["FETCH_MEMO_REQUEST"]:
      return _objectSpread({}, state, {
        memos: state.memos,
        isFetching: true
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["FETCH_MEMO_SUCCESS"]:
      return _objectSpread({}, state, {
        memos: state.memos,
        memo: action.memo,
        isFetching: false
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["FETCH_MEMO_FAILED"]:
      return _objectSpread({}, state, {
        isFetching: false
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["ADD_NEW_MEMO_REQUEST"]:
      return _objectSpread({}, state, {
        memos: state.memos,
        memo: null,
        isFetching: true,
        newMemo: action.memo
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["ADD_NEW_MEMO_REQUEST_FAILED"]:
      return _objectSpread({}, state, {
        memos: state.memos,
        memo: null,
        isFetching: true,
        newTodo: null
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["ADD_NEW_MEMO_REQUEST_SUCCESS"]:
      return _objectSpread({}, state, {
        memos: _toConsumableArray(state.memos).concat([action.memo]),
        memo: null,
        isFetching: false,
        newMemo: action.memo
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["EDIT_MEMO_REQUEST"]:
      return _objectSpread({}, state, {
        memos: state.memos,
        memo: action.memo,
        isFetching: true
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["EDIT_MEMO_SUCCESS"]:
      return _objectSpread({}, state, {
        memos: state.memos.map(function (memo) {
          if (memo._id !== action.memo._id) {
            // This is not the item we care about, keep it as is
            return memo;
          } // Otherwise, this is the one we want to return an updated value


          return _objectSpread({}, memo, action.memo);
        }),
        memo: null,
        isFetching: false
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["EDIT_MEMO_FAILED"]:
      return _objectSpread({}, state, {
        memos: state.memos,
        memo: null,
        isFetching: false,
        memoToEdit: state.memoToEdit
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["DELETE_MEMO_REQUEST"]:
      return _objectSpread({}, state, {
        memos: state.memos,
        memo: action.memo,
        isFetching: true
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["DELETE_MEMO_SUCCESS"]:
      return _objectSpread({}, state, {
        memos: state.memos.filter(function (memo) {
          return memo._id !== state.memo._id;
        }),
        memo: null,
        isFetching: false,
        successMsg: action.message
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["DELETE_MEMO_FAILED"]:
      return _objectSpread({}, state, {
        memos: state.memos,
        todo: null,
        isFetching: false
      });

    default:
      return state;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (memoReducer);

/***/ }),

/***/ "./src/app/containers/notification/constants.js":
/*!******************************************************!*\
  !*** ./src/app/containers/notification/constants.js ***!
  \******************************************************/
/*! exports provided: ADD_NOTIICATION_SUCCESS, ADD_NOTIICATION_FAILED */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_NOTIICATION_SUCCESS", function() { return ADD_NOTIICATION_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_NOTIICATION_FAILED", function() { return ADD_NOTIICATION_FAILED; });
var ADD_NOTIICATION_SUCCESS = 'ADD_NOTIICATION_SUCCESS';
var ADD_NOTIICATION_FAILED = 'ADD_NOTIICATION_FAILED';

/***/ }),

/***/ "./src/app/containers/notification/container.js":
/*!******************************************************!*\
  !*** ./src/app/containers/notification/container.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _notification_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./notification.css */ "./src/app/containers/notification/notification.css");
/* harmony import */ var _notification_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_notification_css__WEBPACK_IMPORTED_MODULE_3__);





var Notification = function Notification(_ref) {
  var message = _ref.message,
      isError = _ref.isError;
  var info = isError ? 'm-message--error' : 'm-message--success';
  return message && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "m-message ".concat(info)
  }, message);
};

Notification.propTypes = {
  message: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string
};

var mapStateToProps = function mapStateToProps(state) {
  var notification = state.ui.notification;
  return {
    message: notification.message,
    isError: notification.isError
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, null)(Notification));

/***/ }),

/***/ "./src/app/containers/notification/create-actions.js":
/*!***********************************************************!*\
  !*** ./src/app/containers/notification/create-actions.js ***!
  \***********************************************************/
/*! exports provided: addNotificationFailed, addNotificationSuccess */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addNotificationFailed", function() { return addNotificationFailed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addNotificationSuccess", function() { return addNotificationSuccess; });
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./constants */ "./src/app/containers/notification/constants.js");

var addNotificationFailed = function addNotificationFailed(error) {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["ADD_NOTIICATION_FAILED"],
    error: error
  };
};
var addNotificationSuccess = function addNotificationSuccess(message) {
  return {
    type: _constants__WEBPACK_IMPORTED_MODULE_0__["ADD_NOTIICATION_SUCCESS"],
    message: message
  };
};

/***/ }),

/***/ "./src/app/containers/notification/notification.css":
/*!**********************************************************!*\
  !*** ./src/app/containers/notification/notification.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./src/app/containers/notification/reducer.js":
/*!****************************************************!*\
  !*** ./src/app/containers/notification/reducer.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./constants */ "./src/app/containers/notification/constants.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var INITIAL_STATE = {
  message: null,
  isError: false
};

var notificationReducer = function notificationReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _constants__WEBPACK_IMPORTED_MODULE_0__["ADD_NOTIICATION_SUCCESS"]:
      return _objectSpread({}, state, {
        message: action.message
      });

    case _constants__WEBPACK_IMPORTED_MODULE_0__["ADD_NOTIICATION_FAILED"]:
      return _objectSpread({}, state, {
        message: action.message,
        isError: true
      });

    default:
      return state;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (notificationReducer);

/***/ }),

/***/ "./src/app/containers/test/container.js":
/*!**********************************************!*\
  !*** ./src/app/containers/test/container.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-config */ "react-router-config");
/* harmony import */ var react_router_config__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_config__WEBPACK_IMPORTED_MODULE_1__);



var Testpage = function Testpage(_ref) {
  var route = _ref.route;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", null, "Child Test"), Object(react_router_config__WEBPACK_IMPORTED_MODULE_1__["renderRoutes"])(route.routes, {
    someProp: 'these extra props are optional'
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Testpage);

/***/ }),

/***/ "./src/app/index.js":
/*!**************************!*\
  !*** ./src/app/index.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-config */ "react-router-config");
/* harmony import */ var react_router_config__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_router_config__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/header */ "./src/app/components/header/index.js");
/* harmony import */ var _components_footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/footer */ "./src/app/components/footer/index.js");
/* harmony import */ var _app_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.css */ "./src/app/app.css");
/* harmony import */ var _app_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_app_css__WEBPACK_IMPORTED_MODULE_5__);


 // import containers





var App = function App(_ref) {
  var route = _ref.route;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "container"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_header__WEBPACK_IMPORTED_MODULE_3__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("main", {
    role: "main"
  }, Object(react_router_config__WEBPACK_IMPORTED_MODULE_2__["renderRoutes"])(route.routes)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_footer__WEBPACK_IMPORTED_MODULE_4__["default"], null));
};

App.propTypes = {
  route: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object
};
/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/app/reducers/index.js":
/*!***********************************!*\
  !*** ./src/app/reducers/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _containers_memos_reducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../containers/memos/reducer */ "./src/app/containers/memos/reducer.js");
/* harmony import */ var _containers_notification_reducer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../containers/notification/reducer */ "./src/app/containers/notification/reducer.js");



var rootReducer = Object(redux__WEBPACK_IMPORTED_MODULE_0__["combineReducers"])({
  memo: _containers_memos_reducer__WEBPACK_IMPORTED_MODULE_1__["default"],
  notification: _containers_notification_reducer__WEBPACK_IMPORTED_MODULE_2__["default"]
});
/* harmony default export */ __webpack_exports__["default"] = (rootReducer);

/***/ }),

/***/ "./src/app/routes.js":
/*!***************************!*\
  !*** ./src/app/routes.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! . */ "./src/app/index.js");
/* harmony import */ var _containers_memo_container__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./containers/memo/container */ "./src/app/containers/memo/container.js");
/* harmony import */ var _containers_memos_container__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./containers/memos/container */ "./src/app/containers/memos/container.js");
/* harmony import */ var _containers_test_container__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./containers/test/container */ "./src/app/containers/test/container.js");




var routes = [{
  component: ___WEBPACK_IMPORTED_MODULE_0__["default"],
  routes: [{
    path: '/',
    exact: true,
    component: _containers_memos_container__WEBPACK_IMPORTED_MODULE_2__["default"]
  }, {
    path: '/memo/:id',
    component: _containers_memo_container__WEBPACK_IMPORTED_MODULE_1__["default"]
  }, {
    path: '/test',
    component: _containers_test_container__WEBPACK_IMPORTED_MODULE_3__["default"]
  }]
}];
/* harmony default export */ __webpack_exports__["default"] = (routes);

/***/ }),

/***/ "./src/app/store/index.js":
/*!********************************!*\
  !*** ./src/app/store/index.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-thunk */ "redux-thunk");
/* harmony import */ var redux_thunk__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_thunk__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var redux_form__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-form */ "redux-form");
/* harmony import */ var redux_form__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(redux_form__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _reducers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../reducers */ "./src/app/reducers/index.js");




var reducer = Object(redux__WEBPACK_IMPORTED_MODULE_0__["combineReducers"])({
  ui: _reducers__WEBPACK_IMPORTED_MODULE_3__["default"],
  form: redux_form__WEBPACK_IMPORTED_MODULE_2__["reducer"]
});

var configureStore = function configureStore(preloadedState) {
  return Object(redux__WEBPACK_IMPORTED_MODULE_0__["createStore"])(reducer, preloadedState, Object(redux__WEBPACK_IMPORTED_MODULE_0__["applyMiddleware"])(redux_thunk__WEBPACK_IMPORTED_MODULE_1___default.a));
};

/* harmony default export */ __webpack_exports__["default"] = (configureStore);

/***/ }),

/***/ "./src/server/controllers/memo.js":
/*!****************************************!*\
  !*** ./src/server/controllers/memo.js ***!
  \****************************************/
/*! exports provided: getMemos, addMemo, updateMemo, getMemo, deleteMemo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMemos", function() { return getMemos; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addMemo", function() { return addMemo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateMemo", function() { return updateMemo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMemo", function() { return getMemo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteMemo", function() { return deleteMemo; });
/* harmony import */ var _models_memo__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../models/memo */ "./src/server/models/memo.js");
// import models

var getMemos = function getMemos(req, res) {
  _models_memo__WEBPACK_IMPORTED_MODULE_0__["default"].find().exec(function (err, memos) {
    if (err) {
      return res.json({
        success: false,
        message: 'Error occured'
      });
    }

    return res.json({
      success: true,
      message: 'Memos fetched successfully',
      memos: memos
    });
  });
};
var addMemo = function addMemo(req, res) {
  var body = req.body;
  var newMemo = new _models_memo__WEBPACK_IMPORTED_MODULE_0__["default"](body);
  newMemo.save(function (err, memo) {
    if (err) {
      return res.json({
        success: false,
        message: 'Error occured'
      });
    }

    return res.json({
      success: true,
      message: 'Memo added successfully',
      memo: memo
    });
  });
};
var updateMemo = function updateMemo(req, res) {
  _models_memo__WEBPACK_IMPORTED_MODULE_0__["default"].findOneAndUpdate({
    _id: req.body._id
  }, req.body, {
    new: true
  }, function (err, memo) {
    if (err) {
      return res.json({
        success: false,
        message: 'Error occured',
        error: err
      });
    }

    return res.json({
      success: true,
      message: 'Updated successfully',
      memo: memo
    });
  });
};
var getMemo = function getMemo(req, res) {
  _models_memo__WEBPACK_IMPORTED_MODULE_0__["default"].find({
    _id: req.params.id
  }).exec(function (err, memo) {
    if (err) {
      return res.json({
        success: false,
        message: 'Error occured'
      });
    }

    if (memo.length) {
      var mem = memo[0];
      return res.json({
        success: true,
        message: 'Memo fetched by id successfully',
        memo: mem
      });
    }

    return res.json({
      success: false,
      message: 'Memo with the given id not found'
    });
  });
};
var deleteMemo = function deleteMemo(req, res) {
  _models_memo__WEBPACK_IMPORTED_MODULE_0__["default"].findByIdAndRemove(req.params.id, function (err) {
    if (err) {
      return res.json({
        success: false,
        message: 'Error occured'
      });
    }

    return res.json({
      success: true,
      message: 'deleted successfully'
    });
  });
};

/***/ }),

/***/ "./src/server/index.js":
/*!*****************************!*\
  !*** ./src/server/index.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dom/server */ "react-dom/server");
/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom_server__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_router_dom_StaticRouter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom/StaticRouter */ "react-router-dom/StaticRouter");
/* harmony import */ var react_router_dom_StaticRouter__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_router_dom_StaticRouter__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_router_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-router-config */ "react-router-config");
/* harmony import */ var react_router_config__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_router_config__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! mongoose */ "mongoose");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _app_routes__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../app/routes */ "./src/app/routes.js");
/* harmony import */ var _app_store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../app/store */ "./src/app/store/index.js");
/* harmony import */ var _route__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./route */ "./src/server/route.js");
 // import path from 'path'





 // import logger from 'morgan'





var app = express__WEBPACK_IMPORTED_MODULE_0___default()(); // allow-cors
// configure app
// serve static files

app.use(express__WEBPACK_IMPORTED_MODULE_0___default.a.static('public')); // set the port

var port = process.env.PORT || 3000;

var renderFullPage = function renderFullPage(html, preloadedState) {
  return "\n      <!doctype html>\n      <html>\n        <head>\n          <title>Memo technical challenge</title>\n          <link rel=\"stylesheet\" href=\"/css/main.css\">\n        </head>\n        <body>\n          <div id=\"root\">".concat(html, "</div>\n          <script>\n            // WARNING: See the following for security issues around embedding JSON in HTML:\n            // http://redux.js.org/recipes/ServerRendering.html#security-considerations\n            window.__PRELOADED_STATE__ = ").concat(JSON.stringify(preloadedState).replace(/</g, "\\u003c"), "\n          </script>\n          <script src=\"/bundle.js\"></script>\n        </body>\n      </html>\n    ");
};

var handleRender = function handleRender(req, res) {
  var context = {}; // Create a new Redux store instance

  var store = Object(_app_store__WEBPACK_IMPORTED_MODULE_8__["default"])(context);
  var branch = Object(react_router_config__WEBPACK_IMPORTED_MODULE_4__["matchRoutes"])(_app_routes__WEBPACK_IMPORTED_MODULE_7__["default"], req.url);
  var promises = branch.map(function (_ref) {
    var route = _ref.route;
    var fetchData = route.component.fetchData;
    return fetchData instanceof Function ? fetchData(store) : Promise.resolve(null);
  });
  return Promise.all(promises).then(function () {
    // Render the component to a string
    var html = Object(react_dom_server__WEBPACK_IMPORTED_MODULE_2__["renderToString"])(react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_5__["Provider"], {
      store: store
    }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_router_dom_StaticRouter__WEBPACK_IMPORTED_MODULE_3___default.a, {
      location: req.url,
      context: context
    }, Object(react_router_config__WEBPACK_IMPORTED_MODULE_4__["renderRoutes"])(_app_routes__WEBPACK_IMPORTED_MODULE_7__["default"])))); // Grab the initial state from our Redux store

    var finalState = store.getState(); // Send the rendered page back to the client

    res.send(renderFullPage(html, finalState));
  });
}; // connect to database


mongoose__WEBPACK_IMPORTED_MODULE_6___default.a.Promise = global.Promise;
mongoose__WEBPACK_IMPORTED_MODULE_6___default.a.connect('mongodb://localhost/memo-app');
app.use('/api', _route__WEBPACK_IMPORTED_MODULE_9__["default"]); // This is fired every time the server side receives a request

app.use(handleRender); // start the server

app.listen(port, function () {
  console.log("App Server Listening at ".concat(port));
});

/***/ }),

/***/ "./src/server/models/memo.js":
/*!***********************************!*\
  !*** ./src/server/models/memo.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongoose */ "mongoose");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);

var MemoSchema = new mongoose__WEBPACK_IMPORTED_MODULE_0__["Schema"]({
  created_date: {
    type: Date,
    default: Date.now
  },
  title: String,
  body: String
});
/* harmony default export */ __webpack_exports__["default"] = (mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.model('Memo', MemoSchema));

/***/ }),

/***/ "./src/server/route.js":
/*!*****************************!*\
  !*** ./src/server/route.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! body-parser */ "body-parser");
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(body_parser__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _controllers_memo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./controllers/memo */ "./src/server/controllers/memo.js");

 // import controller file

 // get an instance of express router

var router = express__WEBPACK_IMPORTED_MODULE_0___default.a.Router(); // parse various different custom JSON types as JSON

router.use(body_parser__WEBPACK_IMPORTED_MODULE_1___default.a.json());
router.use(body_parser__WEBPACK_IMPORTED_MODULE_1___default.a.urlencoded({
  extended: false
}));
router.route('/ideas').get(_controllers_memo__WEBPACK_IMPORTED_MODULE_2__["getMemos"]).post(_controllers_memo__WEBPACK_IMPORTED_MODULE_2__["addMemo"]).put(_controllers_memo__WEBPACK_IMPORTED_MODULE_2__["updateMemo"]);
router.route('/ideas/:id').get(_controllers_memo__WEBPACK_IMPORTED_MODULE_2__["getMemo"]).delete(_controllers_memo__WEBPACK_IMPORTED_MODULE_2__["deleteMemo"]);
/* harmony default export */ __webpack_exports__["default"] = (router);

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "isomorphic-unfetch":
/*!*************************************!*\
  !*** external "isomorphic-unfetch" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ "mongoose":
/*!***************************!*\
  !*** external "mongoose" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-dom/server":
/*!***********************************!*\
  !*** external "react-dom/server" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-dom/server");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "react-router-config":
/*!**************************************!*\
  !*** external "react-router-config" ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-router-config");

/***/ }),

/***/ "react-router-dom":
/*!***********************************!*\
  !*** external "react-router-dom" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-router-dom");

/***/ }),

/***/ "react-router-dom/StaticRouter":
/*!************************************************!*\
  !*** external "react-router-dom/StaticRouter" ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-router-dom/StaticRouter");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "redux-form":
/*!*****************************!*\
  !*** external "redux-form" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-form");

/***/ }),

/***/ "redux-thunk":
/*!******************************!*\
  !*** external "redux-thunk" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-thunk");

/***/ })

/******/ });
//# sourceMappingURL=server.js.map